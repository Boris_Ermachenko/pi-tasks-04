#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<math.h>
#include <stdlib.h>
#define NUM_OF_STR 50
#define LEN_OF_STR 256 
void sort(int *arr1, int *arr2, int count) {
	int i, j, max;
	int k = 0;
	int tmp = 0;
	for (i = 0; i < count; i++) {
		arr2[i] = i;
	}
	for (i = 0; i < count; i++){
		max = arr1[i];
		k = i;
		for (j = i; j < count; j++) 
			if (arr1[j] > max) {
				max = arr1[j];
				k = j;
			}
			tmp = arr1[i];
			arr1[i] = arr1[k];
			arr1[k] = tmp;
			tmp=arr2[i];
			arr2[i] = arr2[k];
			arr2[k] = tmp;
		}
}
int main() {
	char arr[NUM_OF_STR][LEN_OF_STR], ch;
	int arr1[NUM_OF_STR], arr2[NUM_OF_STR];//support arrays
	int n, i, j;
	srand(time(NULL));
	for (i = 0; i < NUM_OF_STR; i++) {
		fgets(arr[i], LEN_OF_STR, stdin);
		if (arr[i][0] == '\n')
			break;
		arr1[i] = rand()%1000;//in this array random values
	}
	n = i;
	sort(arr1, arr2, n);// sort it in the selected order, the second array stores the number given sorting
	for (i = 0; i < n; i++) {
		printf("%s", arr[arr2[i]]);
	}
	return 1;
}